import discord
import asyncio
import datetime
import traceback
import os


async def log_messages(channel, messages):
    max_size = 1000000
    output_file = 'output.txt'
    if os.path.getsize(output_file) > max_size:
        os.rename(output_file,
                  output_file + datetime.now().strftime("-%Y-%m-%d"))
    with open(output_file, 'a+') as out:
        for message in messages:
            out.write(
                f'#{channel} (old) {str(message.created_at)} {message.author}: {message.clean_content}\n')


async def channel_cleanup(b):
    await b.wait_until_ready()
    while (True):
        # Forever
        try:
            for server in b.guilds:
                channel_list = dict((channel.name, channel)
                                    for channel in server.channels)
            # Build a list of the server's channels
            for to_clear in ('personal', 'lgbt-plus'):
                # For each channel we want to trim
                mlist = []
                mlist_old = []
                async for message in channel_list[to_clear].history(limit=None):
                    delta = (datetime.datetime.now(
                        datetime.timezone.utc)-message.created_at)
                    tdiff = delta.days*86400 + delta.seconds
                    if tdiff >= 172800 and tdiff < 1200000:
                        mlist.append(message)
                    elif tdiff >= 1200000:
                        mlist_old.append(message)
                for message in mlist_old:
                    await message.delete()
                    await asyncio.sleep(1)
                for batch_i in range((len(mlist)//100) + 1):
                    batch = mlist[batch_i:batch_i+99]
                    await channel_list[to_clear].delete_messages(batch)
                    await asyncio.sleep(4)
                await log_messages(to_clear, mlist_old + mlist)
        except Exception as e:
            print(repr(e))
            server = b.guilds[0]
            log_chan_cat = [
                cat for cat in server.categories if cat.name.lower() == 'moderation'][0]
            log_chan = [
                chan for chan in log_chan_cat.channels if chan.name.lower() == 'botlog'][0]
            tb = ''.join(traceback.format_tb(e.__traceback__))
            error = discord.utils.escape_markdown(f'{repr(e)}')
            await log_chan.send(f'{error} in trimhistory loop:\n```{tb}```')
        await asyncio.sleep(600)
