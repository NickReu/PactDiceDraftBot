#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import discord
import git
import sys
import traceback
import asyncio
import time
import signal
from discord.ext import commands
from discord.ext import tasks

import dice
import gamechannels
import wounds
import trigger
import trimhistory
import spectate
import autologs
import capes
import snack
import liveread
import townhall
import pd_draft
import hitloc
import messagemin
import characters

path_here = os.path.dirname(os.path.realpath(__file__))
os.chdir(path_here)
# Override the working directory to this file's location


def interrupt(_, __):
    print('\nExited due to SIGINT')
    sys.exit()


signal.signal(signal.SIGINT, interrupt)


class myHelp(commands.DefaultHelpCommand):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.width = 170

    async def on_help_command_error(self, ctx, error):
        await ctx.send('Error sending help message. You must allow messages from server members to receive the help message. \
Change your privacy settings or check https://smileybot.gitlab.io for help.')

    def get_ending_note(self):
        return '```Type `%help <command>` for more info on a command.\nYou can also type `%help <category>` for more info on a category.\n\
For more thorough documentation, go to https://smileybot.gitlab.io (heavily WIP)```'


class SmileyBot(commands.Bot):
    async def setup_hook(self):
        await b.add_cog(capes.Capes())
        await b.add_cog(gamechannels.Game_Channels(b))
        await b.add_cog(dice.Rolls())
        await b.add_cog(wounds.Wounds())
        await b.add_cog(trigger.Triggers_And_More())
        await b.add_cog(autologs.Auto_Logs())
        await b.add_cog(snack.Snacks())
        await b.add_cog(liveread.Liveread())
        await b.add_cog(townhall.Town_Hall(b))
        await b.add_cog(pd_draft.Draft())
        await b.add_cog(hitloc.HitLoc())
        await b.add_cog(messagemin.MessageMin(b))
        await b.add_cog(characters.Characters())
        # Add all the command cogs

        b.loop.create_task(trimhistory.channel_cleanup(b))
        # Start the channel cleanup task on a loop.

        b.loop.create_task(spectate.spectate_topics(b))
        # Run the loop to update spectating channels' topics.


requestedIntents = discord.Intents.default() | discord.Intents(message_content=True)

b = SmileyBot(command_prefix=('%'), case_insensitive=True,
              help_command=myHelp(dm_help=None), intents=requestedIntents)


@b.hybrid_command()
async def hi(ctx: commands.Context) -> None:
    '''The hi command. I'll greet the user.
    '''
    await ctx.send('Hi, <@' + str(ctx.author.id) + '>!')


@b.hybrid_command()
async def updatebot(ctx: commands.Context) -> None:
    '''Updates the bot. Bot team only.'''
    if 'Bot Team' in (str(role) for role in ctx.author.roles):
        async with ctx.typing():
            repo = git.Repo('.')
            repo.remotes.origin.fetch()
            repo.git.reset('--hard', 'origin/main')
            master = repo.head.reference
            message = master.commit.message.split("\n")[0]
            await ctx.send(f'Updated to commit *{message}*. Restarting!', ephemeral=True)
            await ctx.bot.close()
            sys.exit(1)


@b.hybrid_command()
async def sync(ctx: commands.Context) -> None:
    '''Updates application commands. Bot team only.'''
    if 'Bot Team' in (str(role) for role in ctx.author.roles):
        cmd_names = [c.name for c in b.tree.get_commands()]
        if len(cmd_names) == 0:
            await ctx.send(f'Nothing to sync!', ephemeral=True)
            return
        async with ctx.typing(ephemeral=True):
            b.tree.copy_global_to(guild=ctx.guild)
            synced = await b.tree.sync(guild=ctx.guild)
        await ctx.send(f'Syncing...\n> {", ".join(cmd_names)}\nSynced {len(synced)} commands to the server.', ephemeral=True)


@b.hybrid_command()
async def syncglobal(ctx: commands.Context) -> None:
    '''Updates application commands. Bot team only.'''
    if 'Bot Team' in (str(role) for role in ctx.author.roles):
        cmd_names = [c.name for c in b.tree.get_commands()]
        if len(cmd_names) == 0:
            await ctx.send(f'Nothing to sync!', ephemeral=True)
            return
        async with ctx.typing(ephemeral=True):
            synced = await b.tree.sync(guild=None)
        await ctx.send(f'Syncing...\n> {", ".join(cmd_names)}\nSynced {len(synced)} commands globally.', ephemeral=True)


@sync.error
async def sync_error(ctx, error):
    await ctx.send(f'SyncError: {error}', ephemeral=True)


@b.hybrid_command()
async def clearsynclocal(ctx: commands.Context) -> None:
    '''Removes all application commands. Bot team only. Don't use this unless you know why you're doing it!'''
    if 'Bot Team' in (str(role) for role in ctx.author.roles):
        await ctx.send(f'Syncing...', ephemeral=True)
        b.tree.clear_commands(guild=ctx.guild)
        await b.tree.sync(guild=ctx.guild)
        await ctx.send(f'Removed commands.', ephemeral=True)


@b.hybrid_command()
async def clearsyncglobal(ctx: commands.Context) -> None:
    '''Removes all application commands. Bot team only. Don't use this unless you know why you're doing it!'''
    if 'Bot Team' in (str(role) for role in ctx.author.roles):
        await ctx.send(f'Syncing...', ephemeral=True)
        b.tree.clear_commands(guild=None)
        await b.tree.sync(guild=None)
        await ctx.send(f'Removed commands.', ephemeral=True)


async def longsend(channel, content):
    if len(content) > 3900:
        msgs = content.split('\n')
    else:
        msgs = [content]
    found_too_long = True
    while found_too_long:
        found_too_long = False
        for i, msg in enumerate(msgs):
            if len(msg) > 1990:
                found_too_long = True
                msgs[i] = msg[:1990]
                msgs.insert(i+1, msg[1990:])
    for msg in msgs:
        await channel.send(msg)


async def send_dev_cmd_error(ctx, error, msg_override=None):
    try:
        log_chan_cat = [
            cat for cat in ctx.message.guild.categories if cat.name.lower() == "moderation"][0]
        log_chan = [
            chan for chan in log_chan_cat.channels if chan.name.lower() == "botlog"][0]
        escaped_message = ctx.message.content.replace("`", "<backtick>")
        if msg_override:
            escaped_message = msg_override
        cmd = f'<#{ctx.message.channel.id}> **{ctx.author.display_name}**: `{escaped_message}`'
        tb = ''.join(traceback.format_tb(error.original.__traceback__))
        error_text = discord.utils.escape_markdown(f'{repr(error.original)}')
        await longsend(log_chan, f'{error_text} in command:\n{cmd}\n```{tb}```')
    except Exception as e:
        print(e)


async def send_user_cmd_error(ctx, error):
    try:
        escaped_message = ctx.message.content.replace("`", "<backtick>")
        error_text = discord.utils.escape_markdown(f'{str(error)}')
        await ctx.send(f'{error_text} in command: {escaped_message}')
    except Exception as e:
        print(e)


@b.event
async def on_command_error(ctx, error):
    if ctx.message.guild:
        if type(error) == discord.ext.commands.errors.CommandInvokeError:
            await send_dev_cmd_error(ctx, error)
        elif type(error) == discord.ext.commands.errors.HybridCommandError:
            if (type(error.original) == discord.app_commands.errors.CommandInvokeError):
                await send_dev_cmd_error(ctx, error.original, f'/{error.original.command.name} ???')
            else:
                print(type(error.original))
        elif isinstance(error,  discord.ext.commands.errors.UserInputError):
            await send_user_cmd_error(ctx, error)
        else:
            print(repr(error))


with open('secret') as s:
    token = s.read()[:-1]
# Read the Discord bot token from a secret file

while True:
    try:
        b.run(token)
    except Exception as e:
        print(f'Caught exception, will restart bot in 15 seconds: {e}')
    time.sleep(15)
# Start the bot, finally!
