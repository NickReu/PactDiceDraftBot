from discord.ext import commands
from typing import Literal
import sheets
import constants
import discord
import difflib  # Used to find closest name for enter command


class Game_Channels(commands.Cog):
    # - - - - Absolute mess of code below. Mostly channel stuff. Tread at your own risk. - - - -
    def __init__(self, b):
        self.b = b

    @commands.hybrid_command()
    async def campaigns(self, ctx):
        '''Get the link to the campaigns spreadsheet
        '''
        await ctx.send("Campaign list: https://docs.google.com/spreadsheets/d/" + sheets.ID1)

    @commands.hybrid_command()
    async def addgame(self, ctx, game_category: Literal['wd', 'pd'], *, game_name: str):
        '''Create a WD or PD game with you as the GM
        Usage: /addgame <pd/wd> <game name>
        '''
        async with ctx.typing():
            gameMaster = None
            gamecat = None
            altcat = None

            game_name = ''.join(game_name.split(' ')).lower()

            # List of restricted titles
            restricted_names = ['all', 'allactive', 'inactive', 'active', 'wdall',
                                'allwd', 'allpd', 'pdall', 'allarchive', 'archived', 'archives']
            if game_name in restricted_names or 'liveread' in game_name:
                await ctx.send(game_name + " is a restricted term and you can't name your game that. Sorry!")
                return

            for channel in ctx.guild.channels:
                if channel.name == game_name:
                    await ctx.send('There\'s already a channel called ' + game_name + ', use another name to avoid confusion.')
                    return

            if game_name == '':
                await ctx.send("Please write out your game's name after the command (i.e. %addgame pd New York)")
            else:

                for role in ctx.message.guild.roles:
                    if role.name == 'Game Master':
                        gameMaster = role
                    if role.name == "Bot":
                        bot = role
                if gameMaster:
                    await ctx.author.add_roles(gameMaster)

                overwrites = {
                    ctx.guild.default_role: discord.PermissionOverwrite(read_messages=False),
                    ctx.author: discord.PermissionOverwrite(read_messages=True, create_public_threads=True),
                    ctx.me: discord.PermissionOverwrite(read_messages=True),
                    bot: discord.PermissionOverwrite(read_messages=True)
                }
                cats = []

                for category in ctx.guild.categories:
                    if game_category == 'pd' and category.name in constants.pd_categories:
                        cats.append(category)
                    if game_category == 'wd' and category.name in constants.wd_categories:
                        cats.append(category)

                exception = None
                newChannel = None
                for cat in cats:
                    try:
                        newChannel = await ctx.message.guild.create_text_channel(game_name, category=cat, overwrites=overwrites)
                        break
                    except Exception as e:
                        exception = e
                        continue
                if newChannel:
                    server = self.b.guilds[0]
                    channels = [
                        channel for channel in server.channels if channel.category and channel.category == newChannel.category]
                    for i, chan in enumerate(sorted(channels, key=lambda c: c.name)):
                        if newChannel.name < chan.name:
                            await newChannel.edit(position=chan.position)
                            break
                    await sheets.newgame(str('#' + game_name), str(ctx.author.id), str(game_category).upper())
                    await ctx.send(f"{newChannel.mention} has been created in category {newChannel.category.name}")
                else:
                    await ctx.send("Looks like there was a problem creating the channel. If the problem persists, please ping the bot team role", ephemeral=True)
                    raise exception

    @commands.hybrid_command()
    async def enter(self, ctx, game_name: str = '', group: Literal['all', 'all wd', 'all pd', 'all active', 'archives', 'one'] = 'one'):
        '''Join a game channel or group of game channels.
        Usage: /enter <channel>
               /enter <group>
        Group arguments include all wd, all pd, all active, archives, all
        '''
        async with ctx.typing():
            check = False

            game_name = ''.join(game_name.split(' ')).lower()
            if game_name in ['allarchive', 'inactive', 'archived', 'archives', 'pdall', 'allpd', 'all pd', 'wdall', 'allwd', 'all wd', 'all', 'allactive']:
                group = game_name
                game_name = ''

            if game_name == '' and group == 'one':
                await ctx.send("Please choose the game channel(s) you wish to access (i.e. /enter New York)", ephemeral=True)
                return

            joinAllWD = group in ['wdall', 'allwd',
                                  'all wd', 'all', 'allactive', 'all active']
            joinAllPD = group in ['pdall', 'allpd',
                                  'all pd', 'all', 'allactive', 'all active']
            joinAllArchive = group in [
                'all', 'allarchive', 'inactive', 'archived', 'archives']

            applicableChannels = set()

            for channel in ctx.guild.channels:
                if not channel.category:
                    continue
                catName = channel.category.name
                if (catName in constants.wd_categories | constants.pd_categories | constants.archive_categories):
                    applicableChannels.add(channel.name)
                if joinAllWD and catName in constants.wd_categories:
                    await channel.set_permissions(ctx.author, read_messages=True)
                elif joinAllPD and catName in constants.pd_categories:
                    await channel.set_permissions(ctx.author, read_messages=True)
                elif joinAllArchive and catName in constants.archive_categories:
                    await channel.set_permissions(ctx.author, read_messages=True)
                elif channel.name == game_name or 'wd' + channel.name == game_name or 'pd' + channel.name == game_name or '#' + channel.name == game_name:
                    check = (catName in constants.wd_categories |
                             constants.pd_categories | constants.archive_categories)
                    if check:
                        await channel.set_permissions(ctx.author, read_messages=True)
                        await ctx.send('Entered!', ephemeral=True)
                        return

            if not (joinAllWD or joinAllPD or joinAllArchive):
                closestChannels = difflib.get_close_matches(
                    game_name, applicableChannels)
                if len(closestChannels) > 1:
                    await ctx.send("That game could not be found, did you mean one of: " + ', '.join(closestChannels), ephemeral=True)
                elif len(closestChannels) == 1:
                    await ctx.send("That game could not be found, did you mean " + closestChannels[0], ephemeral=True)
                else:
                    await ctx.send("That game could not be found.", ephemeral=True)
            else:
                await ctx.send('Entered!', ephemeral=True)

    @commands.hybrid_command()
    async def exit(self, ctx, game_name: str = '', group: Literal['all', 'all wd', 'all pd', 'all active', 'archives', 'one'] = 'one'):
        '''Leave a game channel
        Usage: %exit <channel>
               %exit <group>
        Group arguments include wdall, pdall, allactive, allarchive, all
        '''
        async with ctx.typing():
            check = False

            game_name = ''.join(game_name.split(' ')).lower()
            if game_name in ['allarchive', 'inactive', 'archived', 'archives', 'pdall', 'allpd', 'all pd', 'wdall', 'allwd', 'all wd', 'all', 'allactive']:
                group = game_name
                game_name = ''

            if game_name == '' and group == 'one':
                await ctx.send("Please choose the game channel(s) you wish to exit (i.e. /exit New York)", ephemeral=True)
                return

            leavewd = group in ['wdall', 'allwd', 'all wd', 'all', 'allactive']
            leavepd = group in ['pdall', 'allpd', 'all pd', 'all', 'allactive']
            leavearchive = group in [
                'all', 'allarchive', 'inactive', 'archived', 'archives']

            for channel in ctx.guild.channels:
                if not channel.category:
                    continue
                # If the channel has no category, move to the next channel
                catName = channel.category.name
                if leavewd and catName in constants.wd_categories:
                    check = True
                    await channel.set_permissions(ctx.author, read_messages=False)
                elif leavepd and catName in constants.pd_categories:
                    check = True
                    await channel.set_permissions(ctx.author, read_messages=False)
                elif leavearchive and catName in constants.archive_categories:
                    check = True
                    await channel.set_permissions(ctx.author, read_messages=False)
                if channel.name == game_name:
                    game = channel
                    check = (catName in constants.wd_categories |
                             constants.pd_categories | constants.archive_categories)
                    if check:
                        await channel.set_permissions(ctx.author, read_messages=False)
                        await ctx.send('Exited!', ephemeral=True)
                        return

            if check == False:
                await ctx.send("That game could not be found.", ephemeral=True)
            else:
                await ctx.send('Exited!', ephemeral=True)

    @commands.hybrid_command()
    async def archive(self, ctx, *, game_name: str):
        '''Move an inactive game to archives
        '''
        game_cat = None
        wd_cats = []
        pd_cats = []
        archive_cats = []

        game_name = ''.join(game_name.split(' ')).lower()

        namecheck = (await sheets.gamecheck(ctx.author.id, game_name))
        moderator = False
        for role in ctx.author.roles:
            if str(role) == 'Mod Team':
                moderator = True
                break

        for category in ctx.message.guild.categories:
            if category.name in constants.pd_categories:
                pd_cats.append(category)
            elif category.name in constants.wd_categories:
                wd_cats.append(category)
            elif category.name in constants.archive_categories:
                archive_cats.append(category)

        for channel in ctx.message.guild.channels:
            if channel.name == game_name:
                game_cat = channel.category

        if game_name == '':
            await ctx.send("Please choose the game you wish to archive (i.e. /archive New York)")
        elif game_cat in archive_cats:
            await ctx.send("That game is already archived.")
        elif namecheck == False and moderator == False:
            await ctx.send("You don't have permission to archive this.")
        else:
            if game_cat not in wd_cats and game_cat not in pd_cats:
                await ctx.send("That game could not be found.")
            else:
                for ctx.TextChannel in ctx.message.guild.text_channels:
                    if ctx.TextChannel.name == game_name:
                        success = False
                        for archive_cat in archive_cats:
                            try:
                                await ctx.TextChannel.edit(category=archive_cat)
                                success = True
                                break
                            except:
                                continue
                        if success:
                            await sheets.changeState(game_name, 'N')
                            await ctx.send('Archived.')
                            return
                        else:
                            await ctx.send("Error, could not archive that game.")

    @commands.hybrid_command()
    async def unarchive(self, ctx, *, game_name: str):
        '''Move an archived game back out of archives
        '''
        gameType = None
        game_cat = None
        wd_cats = []
        pd_cats = []
        archive_cats = []

        for category in ctx.message.guild.categories:
            if category.name in constants.pd_categories:
                pd_cats.append(category)
            elif category.name in constants.wd_categories:
                wd_cats.append(category)
            elif category.name in constants.archive_categories:
                archive_cats.append(category)

        game_name = ''.join(game_name.split(' ')).lower()

        namecheck = (await sheets.gamecheck(ctx.author.id, game_name))
        category = (await sheets.category(game_name))
        moderator = False
        for role in ctx.author.roles:
            if str(role) == 'Mod Team':
                moderator = True
                break

        for channel in ctx.message.guild.channels:
            if channel.name == game_name:
                game_cat = channel.category

        if game_name == '':
            await ctx.send("Please write out the game you wish to unarchive after the command (i.e. %unarchive New York)")
        elif game_cat in wd_cats or game_cat in pd_cats:
            await ctx.send("That game is already active.")
        elif namecheck == False and moderator == False:
            await ctx.send("You don't have permission to unarchive this.")
        else:
            if game_cat not in archive_cats:
                await ctx.send("That game could not be found.")
            else:
                for ctx.TextChannel in ctx.message.guild.text_channels:
                    if ctx.TextChannel.name == game_name:
                        if gameType is None:
                            gameType = category.lower()
                        if gameType == 'pd':
                            for pd_cat in pd_cats:
                                try:
                                    await ctx.TextChannel.edit(category=pd_cat)
                                    success = True
                                    break
                                except:
                                    continue
                        elif gameType == 'wd':
                            for wd_cat in wd_cats:
                                try:
                                    await ctx.TextChannel.edit(category=wd_cat)
                                    success = True
                                    break
                                except:
                                    continue
                        if success:
                            await sheets.changeState(game_name, 'Y')
                            await ctx.send('Unarchived.')
                        else:
                            await ctx.send("Error, could not unarchive that game.")

    @commands.hybrid_command()
    async def link(self, ctx, *, topic: str):
        '''Sets the channel topic and the link on the spreadsheet for your game.
        Usage: /link <topic information and links>
        Note that you have to be the GM of the channel.
        '''

        c = ctx.message.channel

        failure = await sheets.addlink(ctx.author.id, c.name, topic)
        if failure:
            await ctx.send('Error editing topic, make sure you have the name right and you\'re the GM')
        else:
            # Let's make this the topic
            topic = "GM is <@{}> | ".format(ctx.author.id) + topic
            await c.edit(topic=topic)
            await ctx.send('Added.', ephemeral=True)

    @commands.command()
    async def pin(self, ctx):
        '''Sets the message you are posting to be pinned to the channel if you are the owner of the channel.
        Usage: %pin This is the message that is being pinned. Cannot be used as a slash command.
        You can edit the message after the fact.
        '''
        c = ctx.message.channel
        owner = None
        if isinstance(c, discord.Thread):
            owner = await sheets.ownercheck(c.parent.name)
        else:
            owner = await sheets.ownercheck(c.name)
        if owner == str(ctx.author.id):
            await ctx.message.pin()
        else:
            await ctx.send("You are not the owner of this channel and cannot pin this message")

    @commands.hybrid_command()
    async def unpin(self, ctx, link):
        '''Unpins a message from your game's channel.
        Usage: /unpin <link to message>
        '''
        target_message = await ctx.fetch_message(int(link.split('/')[-1]))
        c = target_message.channel
        owner = None
        if isinstance(c, discord.Thread):
            owner = await sheets.ownercheck(c.parent.name)
        else:
            owner = await sheets.ownercheck(c.name)
        if owner == str(ctx.author.id):
            await target_message.unpin()
        else:
            await ctx.send("You are not the owner of that channel and cannot unpin any messages")

    @commands.hybrid_command()
    async def owner(self, ctx, *, game_name: str):
        '''Checks the owner of a given campaign
        Usage: %owner <game name>
        '''
        game_name = ''.join(game_name.split(' ')).lower()

        ownercheck = (await sheets.ownercheck(game_name))

        if ownercheck != '':
            await ctx.send('<@' + ownercheck + '> owns ' + game_name)
        else:
            await ctx.send('Could not find game ' + game_name)
