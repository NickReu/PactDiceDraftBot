import discord
from discord.ext import commands
from discord.utils import get
import json


async def longsend(channel, content):
    if len(content) > 1990:
        msgs = content.split('\n')
    else:
        msgs = [content]
    found_too_long = True
    while found_too_long:
        found_too_long = False
        for i, msg in enumerate(msgs):
            if len(msg) > 1990:
                found_too_long = True
                msgs[i] = msg[:1990]
                msgs.insert(i+1, msg[1990:])
    for msg in msgs:
        await channel.send(msg)


class Town_Hall(commands.Cog):
    # Gonna do my best here
    def __init__(self, b):
        self.b = b
        self.chan = 874401694287536238
        self.open = True
        try:
            with open('blacklist.json', 'r+') as bl:
                self.blacklist = json.load(bl)
        except FileNotFoundError:
            with open('blacklist.json', 'w+') as bl:
                json.dump([], bl)
                self.blacklist = []

    async def send_town_hall_msg(self, ctx, kind, msg):
        try:
            if not self.open:
                await ctx.send('Sorry, the town hall has not been opened by the moderators.', ephemeral=True)
                return

            if str(ctx.author.id) in self.blacklist:
                await ctx.send('Sorry, you are not allowed to send messages to the town hall.', ephemeral=True)
                return
            channel = self.b.get_channel(self.chan)
            server = self.b.guilds[0]
            mod_team = get(server.roles, name='Mod Team')
            ping = f'{mod_team.mention} - ' if kind == 'Report' else ''
            channelInfo = ''
            if isinstance(ctx.channel, discord.TextChannel) or isinstance(ctx.channel, discord.Thread):
                channelInfo = f' in <#{ctx.channel.id}>'
            elif isinstance(ctx.channel, discord.DMChannel):
                channelInfo = f' via DM'
            full_msg = f'{ping}{kind} from <@!{ctx.author.id}>{channelInfo}:\n{msg}'
            await longsend(channel, full_msg)
            await ctx.send('Message sent. Thanks!', ephemeral=True)
        except Exception as e:
            await ctx.send(f'Error sending message: {e}\nPlease try again or message a moderator directly.', ephemeral=True)

    @commands.hybrid_command()
    async def openhall(self, ctx):
        '''Mod command only. Opens the town hall for communication in a given channel.
        '''
        if ('Mod Team' not in (str(role) for role in ctx.author.roles)) and ('Author' not in (str(role) for role in ctx.author.roles)) and ('Bot Team' not in (str(role) for role in ctx.author.roles)):
            await ctx.send('Only moderators may use this command.', ephemeral=True)
            return

        if self.open:
            await ctx.send('The town hall is open elsewhere.', ephemeral=True)
            return

        self.open = True
        self.chan = ctx.channel.id
        await ctx.send('Town hall now open.')

    @commands.hybrid_command()
    async def closehall(self, ctx):
        '''Mod command only. Closes the town hall for communication in a given channel.
        '''
        if ('Mod Team' not in (str(role) for role in ctx.author.roles)) and ('Author' not in (str(role) for role in ctx.author.roles)) and ('Bot Team' not in (str(role) for role in ctx.author.roles)):
            await ctx.send('Only moderators may use this command.', ephemeral=True)
            return

        if not self.open:
            await ctx.send('The town hall is not open.', ephemeral=True)
            return

        self.open = False
        self.chan = None
        await ctx.send('Town hall now closed.')

    @commands.hybrid_command()
    async def report(self, ctx, *, msg: str):
        '''Send a report straight to the mods. Not anonymous!
        '''
        await self.send_town_hall_msg(ctx, 'Report', msg)

    @commands.hybrid_command()
    async def feedback(self, ctx, *, msg: str):
        '''Send feedback straight to the mods. Not anonymous!
        '''
        await self.send_town_hall_msg(ctx, 'Feedback', msg)

    @commands.hybrid_group()
    async def blacklist(self, ctx, *args):
        '''Mod command only. Blocks a user from sending messages to the town hall.
        '''
        pass

    @blacklist.command()
    async def show(self, ctx):
        if ('Mod Team' not in (str(role) for role in ctx.author.roles)) and (
                'Author' not in (str(role) for role in ctx.author.roles)) and (
                'Bot Team' not in (str(role) for role in ctx.author.roles)):
            await ctx.send('Only moderators may use this command.', ephemeral=True)
            return
        message = 'Current names in blacklist:\n'
        for user_id in self.blacklist:
            message += f'<@!{user_id}>\n'
        await ctx.send(message, ephemeral=True)

    @blacklist.command()
    async def remove(self, ctx, user: str):
        if ('Mod Team' not in (str(role) for role in ctx.author.roles)) and (
                'Author' not in (str(role) for role in ctx.author.roles)) and (
                'Bot Team' not in (str(role) for role in ctx.author.roles)):
            await ctx.send('Only moderators may use this command.', ephemeral=True)
            return
        if user[3:-1] in self.blacklist:
            self.blacklist.remove(user[3:-1])
            with open('blacklist.json', 'w+') as bl:
                json.dump(self.blacklist, bl)
            await ctx.send(f'Removed {user} from blacklist', ephemeral=True)
        else:
            await ctx.send(f'{user} not found on blacklist.', ephemeral=True)
            return

    @blacklist.command()
    async def add(self, ctx, user: str):
        if ('Mod Team' not in (str(role) for role in ctx.author.roles)) and (
                'Author' not in (str(role) for role in ctx.author.roles)) and (
                'Bot Team' not in (str(role) for role in ctx.author.roles)):
            await ctx.send('Only moderators may use this command.', ephemeral=True)
            return
        if user[3:-1] in self.blacklist:
            await ctx.send(f'{user} is already on the blacklist.', ephemeral=True)
            return
        self.blacklist.append(user[3:-1])
        with open('blacklist.json', 'w+') as bl:
            json.dump(self.blacklist, bl)
        await ctx.send(f'Added {user} to blacklist.', ephemeral=True)
