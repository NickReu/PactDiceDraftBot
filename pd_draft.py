from itertools import chain
from typing import Literal

from discord.ext import commands
import discord
import random
import asyncio
import numpy
import sheets


class Draft(commands.Cog):
    '''Commands for Pact Dice alt drafts
    '''

    def __init__(self):
        self.current_ID = None  # ID of sheet used in draft
        self.lock = asyncio.Lock()
        # Claims players will make in a given round. Keys are player ids, items are an array
        self.claims = {}
        self.players = []  # player IDs
        self.player_names = []  # display names of players, in same order as IDs
        self.hands = {}  # What each player has, to avoid them bidding on something they already have
        for p in self.players:
            # Sets each player as the key, each hand is an array
            self.hands[p] = []
        # Detected clashes are placed here as arrays of the clashing players
        self.clash_queue = []
        self.wm_queue = []
        self.bm_queue = []
        self.clash_claims = {}  # Holds what each player is claiming in a clash
        self.gray_card_locations = set()  # Where are gray cards?
        self.ctx = None  # Context of the channel its deployed in
        self.all_forfeit = False  # Flag for if all players in a clash forfeited
        self.split = False  # Flag for if players are split hold/forfeit-wise
        # Flag for if a forfeiting player must make a choice - enables /choose command
        self.decision = False
        self.choosers = []  # List of player IDs who have to make choices
        # Determines what type of choice they have to make, depending on draft situation
        self.choose_type = 0
        self.contested = []  # Holds data on the exact squares that are contested in a given clash
        self.targets = []  # Holds the IDs of viable player targets for black mark dispersal
        self.deck = []  # Holds the deck of cards to draw from in a draft
        self.game_time = False  # Blocker from gibbing before or after a draft
        self.gib_time = True  # Blocker for gibbing when not time to gib
        self.adding_time = False  # Blocker for joining in draft if already underway
        self.no_new = False  # Quick check for if a draft is running, to prevent new drafts
        self.help = {
            'gib': 'Makes a bid at a given row or column during the draft, i.e. `/draft gib puissance e`.',
            'clash': '''Used during clashes to submit choices.
Can input either `/draft clash hold` or `/draft clash forfeit` for basic results, or can input exact bids after negotiations and division with an opponent. I.e. `/draft clash split a1 draw c1 D1 draw`''',
            'choose': 'Allows one to make a choice after forfeiting in a clash.',
            'lesgo': 'Begins draft once all players are in through `/draft add_me`.',
            'generate': 'Generates a draft sheet and sets up a draft.',
            'add_me': 'Adds a user from a current draft.',
            'remove_me': 'Removes a user from a current draft.',
            'reset': 'Entirely resets current draft.',
            'active': 'View the active draft.',
            'draft': '''Alt/new draft method for Pact Dice. Use with subcommands.
Usage:
/draft gib <category> <row or column>
/draft clash <hold/forfeit>
/draft clash split <option> <option> <option> <option> <option>
/draft choose <choice>
/draft lesgo
/draft generate
/draft add_me
/draft remove_me
/draft reset
/draft active
/draft help <subcommand>

Use /draft help <subcommand> for more help.
'''
        }

    @commands.hybrid_group()
    async def draft(self, ctx, cmd, *args):
        '''New draft method for Pact Dice. Use with subcommands.
        Usage:
        /draft gib <category> <row or column>
        /draft clash <hold/forfeit>
        /draft clash split <option> <option> <option> <option> <option>
        /draft choose <choice>
        /draft lesgo
        /draft generate
        /draft add_me
        /draft remove_me
        /draft reset
        /draft help <subcommand>

        Use /draft help <subcommand> for more help.
        '''
        pass

    async def do_reset(self):
        self.current_ID = None
        self.claims = {}
        self.players = []
        self.player_names = []
        self.hands = {}
        for p in self.players:
            self.hands[p] = []
        self.clash_queue = []
        self.wm_queue = []
        self.bm_queue = []
        self.clash_claims = {}
        self.gray_card_locations = set()
        self.ctx = None
        self.all_forfeit = False
        self.split = False
        self.decision = False
        self.choosers = []
        self.choose_type = 0
        self.contested = []
        self.targets = []
        self.deck = []
        self.game_time = False
        self.gib_time = True
        self.adding_time = False
        self.no_new = False

    @draft.command()
    async def reset(self, ctx=None):
        '''Resets current draft entirely.
        '''
        await self.do_reset()
        await ctx.send('Reset!')

    async def load_sheet(self, ctx=None):
        '''Loads in a sheet through the sheet ID in the url.
        '''
        ranges = ['C24', 'J24', 'Q24', 'C32', 'J32', 'Q32', 'C40',
                  'J40', 'Q40']  # The exact squares where names can be
        if self.current_ID is None and ctx is not None:
            await ctx.send("No.")
        else:
            output = await sheets.read_sheet(self.current_ID, ranges)
            if "values" in output:
                self.players = output["values"]
            self.hands = {}
            for p in self.players:
                self.hands[p] = []
            self.claims = {}

            # Not sure this works:
            #
            # o = await sheets.get_sheet_colors(self.current_ID)
            # counter = 0
            # row = 0
            # box = 0
            # level = 0
            # while row != 10:
            #     self.grid[row].append(o[counter])
            #     counter += 1
            #     if len(self.grid[row]) >= (box * 5) + 5:
            #         row += 1
            #     if row >= (level * 5) + 5:
            #         box += 1
            #         row = level * 5
            #     if box > 2:
            #         box = 0
            #         level += 1
            #         row = level * 5

            self.deck = await sheets.get_deck(self.current_ID)

    @draft.command()
    async def generate(self, ctx):
        '''Generates a draft sheet and sets up a draft.
        '''
        if self.no_new == True:
            await ctx.send("Another game is running. Reset or wait for that one to finish, please.")
            return
        if isinstance(ctx.channel, discord.DMChannel) or ctx.channel.type == discord.ChannelType.private or ctx.channel.type == discord.ChannelType.private_thread:
            await ctx.send("You can't draft in direct messages!")
            return
        await ctx.send("Generating sheet... (This may take a minute!)")
        self.current_ID = await sheets.new_alt_sheet()
        await ctx.send('Now running: https://docs.google.com/spreadsheets/d/' + self.current_ID)
        await ctx.send('Players may join via the `/draft add_me` command. Begin the game with `/draft lesgo`.')
        await self.load_sheet()
        self.adding_time = True
        self.no_new = True
        self.ctx = ctx

    # Sets players in program based on what's on the sheet. dont worry bout it
    async def update_players(self):
        request = []
        ranges = [27, 2, 27, 9, 27, 16, 35, 2,
                  35, 9, 35, 16, 43, 2, 43, 9, 43, 16]
        marker = 0
        self.hands = {}
        for player in self.player_names:
            r = {"repeatCell": {"range": {"sheetId": 0, "startRowIndex": ranges[marker],
                                          "endRowIndex": ranges[marker] + 1, "startColumnIndex": ranges[marker + 1],
                                          "endColumnIndex": ranges[marker + 1] + 1},
                                "cell": {"userEnteredValue": {"stringValue": player}},
                                "fields": "userEnteredValue(stringValue)"}}
            request.append(r)
            marker += 2
        for p in self.players:
            self.hands[p] = []
        await sheets.update_sheet(self.current_ID, request)

    @draft.command()
    async def add_me(self, ctx):
        '''Adds user to a current draft, if one is set up via `/draft generate`.
        '''
        async with self.lock:
            if self.adding_time == False:
                await ctx.send("no.")
                return
            if str(ctx.author.id) not in self.players:
                self.players.append(str(ctx.author.id))
                self.player_names.append(ctx.author.display_name)
                await self.update_players()
                await ctx.send("Added " + ctx.author.display_name)
            else:
                await ctx.send("Nuh-uh.")

    @draft.command()
    async def remove_me(self, ctx):
        '''Removes a user from a current draft.
        '''
        async with self.lock:
            if self.adding_time == False:
                await ctx.send("no.")
                return
            if str(ctx.author.id) in self.players:
                self.players.remove(str(ctx.author.id))
                await self.update_players()
                await ctx.send("Removed " + ctx.author.display_name)
            else:
                await ctx.send("I refuse.")

    @draft.command()
    async def lesgo(self, ctx):  # For the record, I consider "lesgo" to be one word
        '''Begins draft once all players are in through `/draft add_me`.
        '''
        async with self.lock:
            if len(self.players) < 3:
                await ctx.send("You need at least 3 players.")
                return
            else:
                self.adding_time = False
                await ctx.send("Beginning draft. Please input bids through the `/draft gib` command, i.e. `/draft gib puissance a`.")
                self.gib_time = True
                self.game_time = True
                self.no_new = True

    async def handle_deferred_marks(self):
        for args in self.wm_queue:
            await sheets.add_white_mark(self.current_ID, *args)
        for args in self.bm_queue:
            await sheets.send_black_mark(self.current_ID, *args)
        self.wm_queue = []
        self.bm_queue = []

    # Handles the end of the round, checks to see if draft is over, if not, continues
    async def round_end(self):
        weights = [0, 0, 0, 0, 0, 0]
        for p in list(self.players):
            self.hands[p].append(self.claims[p][0])
            newWeights = await sheets.transfer_hand(self.claims[p], self.players.index(p), self.current_ID, self.gray_card_locations,
                                                    self.deck, self.player_names[self.players.index(p)], self.ctx)
            weights = [x + y for x, y in zip(weights, newWeights)]
        await self.handle_deferred_marks()
        await sheets.adjust_deck(self.current_ID, vals=weights)
        self.deck = await sheets.get_deck(self.current_ID)
        self.claims = {}
        self.clash_queue = []
        self.clash_claims = {}
        if len(self.hands[self.players[0]]) == 6:
            await self.ctx.send("Draft complete. Have a nice day.")
            await self.do_reset()
        else:
            await self.ctx.send("Round complete. Submit your next bids, please.")
            self.gib_time = True

    @draft.command()
    async def choose(self, ctx, first_choice: Literal['1', '2', '3'], second_choice: Literal['1', '2', '3', 'n/a'] = 'n/a'):
        '''Allows one to make a choice after forfeiting in a clash.
        '''
        async with self.lock:
            if self.choose_type > 0 and str(ctx.author.id) in self.choosers and first_choice:
                if self.choose_type == 1:
                    if first_choice == '1':
                        newClaim = [
                            self.claims[list(self.clash_claims.keys())[0]][0]]
                        for claim in self.claims[str(ctx.author.id)][1:]:
                            if claim not in self.contested:
                                newClaim.append(claim)
                            else:
                                newClaim.append('draw')
                        newClaim.append('draw')
                        self.claims[str(ctx.author.id)] = newClaim
                        self.choosers.remove(str(ctx.author.id))
                        await ctx.send("Understood.")
                        if len(self.choosers) == 0:
                            self.clash_queue.pop(0)
                            self.decision = False
                            self.clash_claims = {}
                            self.all_forfeit = False
                            self.choose_type = 0
                            self.contested = []
                            if self.clash_queue:
                                await self.clashing()
                            else:
                                await self.round_end()
                    elif first_choice == '2':
                        newClaim = [
                            self.claims[list(self.clash_claims.keys())[0]][0]]
                        for claim in self.claims[str(ctx.author.id)][1:]:
                            if claim not in self.contested:
                                newClaim.append(claim)
                            else:
                                newClaim.append('draw')
                        self.claims[str(ctx.author.id)] = newClaim
                        await ctx.send("Understood. Flipping coin...")
                        coin = random.randint(0, 1)
                        if coin == 0:
                            await ctx.send("No dice. No white mark granted.")
                        else:
                            await ctx.send("Got it. White mark will be granted at the end of the round.")
                            self.wm_queue.append((self.players.index(str(ctx.author.id)),
                                                  self.claims[str(ctx.author.id)][0]))
                        self.choosers.remove(str(ctx.author.id))
                        if len(self.choosers) == 0:
                            self.clash_queue.pop(0)
                            self.decision = False
                            self.clash_claims = {}
                            self.all_forfeit = False
                            self.choose_type = 0
                            self.contested = []
                            if self.clash_queue:
                                await self.clashing()
                            else:
                                await self.round_end()

                elif self.choose_type == 2:
                    if second_choice != 'n/a':
                        if first_choice == '3' and second_choice == '3':
                            await ctx.send("Absolutely not.")
                        else:
                            await ctx.send("Understood.")
                            draws = 0
                            black = 0
                            white = False
                            for arg in first_choice, second_choice:
                                if arg == '1':
                                    draws += 1
                                elif arg == '2':
                                    black += 1
                                else:
                                    white = True
                            newClaim = [self.claims[list(self.clash_claims.keys())[0]][
                                0]]  # returns clash category, to go in new claim
                            for claim in self.claims[str(ctx.author.id)][1:]:
                                if claim not in self.contested:
                                    newClaim.append(claim)
                                else:
                                    newClaim.append('draw')
                            for x in range(draws):
                                newClaim.append('draw')
                            self.claims[str(ctx.author.id)] = newClaim
                            if black:
                                coin = random.randint(0, 2 - black)
                                if coin == 1:
                                    await ctx.send("No dice. No black mark sent.")
                                else:
                                    unlucky = random.choice(self.targets)
                                    await ctx.send(f"Got it. A black mark will be sent to {self.player_names[self.players.index(unlucky)]} at the end of the round.")
                                    self.bm_queue.append(
                                        (self.players.index(unlucky), self.claims[str(ctx.author.id)][0]))
                            if white:
                                await ctx.send("White mark will be granted at the end of the round.")
                                self.wm_queue.append((self.players.index(str(ctx.author.id)),
                                                      self.claims[str(ctx.author.id)][0]))
                            self.clash_queue[0].remove(str(ctx.author.id))
                            self.choosers.remove(str(ctx.author.id))
                            if len(self.choosers) == 0:
                                if len(self.clash_queue[0]) < 2:
                                    self.clash_queue.pop(0)
                                self.decision = False
                                self.clash_claims = {}
                                self.all_forfeit = False
                                self.choose_type = 0
                                self.contested = []
                                if self.clash_queue:
                                    await self.clashing()
                                else:
                                    await self.round_end()
                    else:
                        await ctx.send("You must make two valid choices")
            else:
                await ctx.send("It's either not time for you to choose or you didn't put a valid number in.")

    async def handle_binary_clash(self):
        for key in list(self.clash_claims.keys()):
            await self.ctx.send(
                self.player_names[self.players.index(key)] + " chose to " + self.clash_claims[
                    key] + ".")
        if all(v == 'hold' for v in list(self.clash_claims.values())):
            contested = []
            cat = self.claims[list(
                self.clash_claims.keys())[0]][0]
            for x in range(len(self.clash_claims.keys())):
                for y in range(x + 1, len(self.clash_claims.keys())):
                    for val in set(self.claims[list(self.clash_claims.keys())[x]][1:]).intersection(
                            self.claims[list(self.clash_claims.keys())[y]][1:]):
                        contested.append(val)
            random.shuffle(contested)
            await sheets.add_black_mark(self.current_ID, cat, contested[0])
            self.clash_claims = {}
            await self.ctx.send(
                "No forfeits. Black mark added. Please re-negotiate and/or resubmit choices via `/draft clash`.")
            return
        elif all(v == 'forfeit' for v in list(self.clash_claims.values())):
            self.contested = []
            for x in range(len(self.clash_claims.keys())):
                for y in range(x + 1, len(self.clash_claims.keys())):
                    for val in set(self.claims[list(self.clash_claims.keys())[x]][1:]).intersection(
                            self.claims[list(self.clash_claims.keys())[y]][1:]):
                        self.contested.append(val)
            self.all_forfeit = True
            self.decision = True
            self.choosers = self.clash_queue[0].copy()
            self.choose_type = 1
            await self.ctx.send(
                "All forfeited. All will take gray cards instead of those contested.")
            await self.ctx.send(
                "All involved may choose one of the following:\n1. Draw an extra gray card and discard the worst result\n2. A 50% chance of a white mark.")
            await self.ctx.send(
                "Please submit your choice via `/draft choose 1` or `/draft choose 2`")
            return
        else:
            self.contested = []
            self.targets = []
            for x in range(len(self.clash_claims.keys())):
                for y in range(x + 1, len(self.clash_claims.keys())):
                    for val in set(self.claims[list(self.clash_claims.keys())[x]][1:]).intersection(
                            self.claims[list(self.clash_claims.keys())[y]][1:]):
                        self.contested.append(val)
            if (len(self.contested) == 1):
                self.choose_type = 1
            else:
                self.choose_type = 2
            self.split = True
            self.decision = True
            for key, val in self.clash_claims.items():
                if val.lower() == 'forfeit':
                    self.choosers.append(key)
                else:
                    self.targets.append(key)
            msg = "The following players forfeited:"
            for player in self.choosers:
                msg += "\n" + \
                    self.player_names[self.players.index(player)]
            if (len(self.contested) == 1):
                msg += "\nThese players may choose __one__ of the following:\n" + \
                    "1. Draw another gray card and discard the worst result.\n" + \
                    "2. A 50% chance of a white mark.\n" + \
                    "Please submit your choice via `/draft choose`, with one argument. I.e. `/draft choose 2`"
            else:
                msg += f"\nThese players may choose __two__ of the following:\n" + \
                    "1. Draw another gray card and discard the worst result. They may do this twice.\n" + \
                    "2. Give the other player (random holder if multiple) a 50% shot at gaining a black mark, flat 100% if this option is taken twice.\n" + \
                    "3. Gain a white mark.\n" + \
                    "Please submit your choice via `/draft choose`, with two arguments. I.e. `/draft choose 1 3`"
            await self.ctx.send(msg)

    async def handle_clash(self):
        if len(self.clash_claims.keys()) == len(self.clash_queue[0]):
            if all(type(claim) == str for claim in self.clash_claims.values()):
                await self.handle_binary_clash()
            elif all(type(claim) == tuple for claim in self.clash_claims.values()):
                for l in list(self.clash_claims.values()):
                    for val in l:
                        val = val.lower()
                check = list(set.intersection(
                    *map(set, list(self.clash_claims.values()))))
                if check != ['draw']:
                    await self.ctx.send(
                        "Inputs contained competing claims. Please resume negotiations and/or re-input results.")
                    return
                else:
                    for key in list(self.clash_claims.keys()):
                        newClaim = [self.claims[key][0]]
                        for claim in self.clash_claims[key]:
                            newClaim.append(claim)
                        self.claims[key] = newClaim
                    await self.ctx.send("Negotiation results successfully recorded.")
                    self.clash_queue.pop(0)
                    self.clash_claims = {}
                    if self.clash_queue:
                        await self.clashing()
                    else:
                        await self.round_end()
            else:
                await self.ctx.send("It seems at least one person has split without everyone doing so. Unfortunately, mixing forfeits and holds with splits is not supported right now. Please ensure everyone splits or no one splits.")

    @draft.group()
    async def clash(self, ctx):
        '''Used during clashes to submit choices.
Can input either `/draft clash hold` or `/draft clash forfeit` for basic results, or can input exact bids after negotiations and division with an opponent. I.e. `/draft clash split a1 draw c1 D1 draw`'''
        pass

    @clash.command()
    async def hold(self, ctx):
        '''Refuse to back down even one inch - this is the road to a lot of enemies'''
        async with self.lock:
            if self.decision == False:
                if self.clash_queue and (str(ctx.author.id) in self.clash_queue[0]):
                    self.clash_claims[str(ctx.author.id)] = 'hold'
                    await ctx.send("Understood.", ephemeral=True)
                    await self.handle_clash()
                    return
            await ctx.send(f'I\'m sorry {ctx.author.display_name}, I\'m afraid I can\'t do that.', ephemeral=True)

    @clash.command()
    async def forfeit(self, ctx):
        '''Back down completely - this is the road to a lot of gray cards'''
        async with self.lock:
            if self.decision == False:
                if self.clash_queue and (str(ctx.author.id) in self.clash_queue[0]):
                    self.clash_claims[str(ctx.author.id)] = 'forfeit'
                    await ctx.send("Understood.", ephemeral=True)
                    await self.handle_clash()
                    return
            await ctx.send(f'I\'m sorry {ctx.author.display_name}, I\'m afraid I can\'t do that.', ephemeral=True)

    @clash.command()
    async def split(self, ctx, first: str, second: str, third: str, fourth: str, fifth: str):
        '''Negotiate with your fellow clasher to split up the cards'''
        async with self.lock:
            if self.decision == False:
                if self.clash_queue and (str(ctx.author.id) in self.clash_queue[0]):
                    flag = True
                    args = (first, second, third, fourth, fifth)
                    for arg in args:
                        if arg.lower() != 'draw':
                            if arg.lower() not in self.claims[str(ctx.author.id)]:
                                flag = False
                    if flag:
                        self.clash_claims[str(ctx.author.id)] = args
                        await ctx.send("Understood.", ephemeral=True)
                        await self.handle_clash()
                        return
                    else:
                        await ctx.send("Invalid input. Try, try again~", ephemeral=True)
                        return
            await ctx.send(f'I\'m sorry {ctx.author.display_name}, I\'m afraid I can\'t do that.', ephemeral=True)

    async def clashing(self):  # Handles messages once a clash is found.
        msg = "Clash detected between the following players:"
        for player in self.clash_queue[0]:
            msg += "\n" + \
                self.player_names[self.players.index(player)] + " - **"
            for claim in self.claims[player]:
                msg += claim.upper() + ", "
            msg = msg[:-2]
            msg += "**"
        msg += "\nPlease negotiate amongst yourselves. When finished, use command `/draft clash` to input final result."
        msg += "\nIn the case of a standoff, submit `/draft clash hold` or `/draft clash forfeit`."
        msg += "\nIn the case of a resolution, submit the slots of the final cards taken, signifying deck draws with \'Draw\'."
        msg += "\nExample: `/draft clash split a1 draw d1 e1 draw`"
        await self.ctx.send(msg)

    # Resolves bidding by determining if there are any clashes among given bids.
    async def resolve(self):
        counter = 1
        for x in self.players:
            if counter != len(self.players):
                for y in self.players[counter:]:
                    if self.claims[x][0] == self.claims[y][0]:
                        if set(self.claims[x][1:]).intersection(self.claims[y][1:]):
                            if x in chain(*self.clash_queue) or y in chain(*self.clash_queue):
                                for clash in self.clash_queue:
                                    if x in clash or y in clash:
                                        clash.append(x)
                                        clash.append(y)
                                        self.clash_queue[self.clash_queue.index(
                                            clash)] = list(set(clash))
                            else:
                                self.clash_queue.append([x, y])
            counter += 1

        if self.clash_queue:
            await self.clashing()
        else:
            await self.round_end()

    def get_claim(self, cat, input):  # Converts a claim into another format, basic data management
        out = []
        out.append(cat[0])
        if input == 'a':
            out.extend(['a1', 'a2', 'a3', 'a4', 'a5'])
        elif input == 'b':
            out.extend(['b1', 'b2', 'b3', 'b4', 'b5'])
        elif input == 'c':
            out.extend(['c1', 'c2', 'c3', 'c4', 'c5'])
        elif input == 'd':
            out.extend(['d1', 'd2', 'd3', 'd4', 'd5'])
        elif input == 'e':
            out.extend(['e1', 'e2', 'e3', 'e4', 'e5'])
        elif input == '1':
            out.extend(['a1', 'b1', 'c1', 'd1', 'e1'])
        elif input == '2':
            out.extend(['a2', 'b2', 'c2', 'd2', 'e2'])
        elif input == '3':
            out.extend(['a3', 'b3', 'c3', 'd3', 'e3'])
        elif input == '4':
            out.extend(['a4', 'b4', 'c4', 'd4', 'e4'])
        elif input == '5':
            out.extend(['a5', 'b5', 'c5', 'd5', 'e5'])

        return out

    @draft.command()
    async def gib(self, ctx, category: Literal['puissance', 'access', 'longevity', 'executions', 'schools', 'family'],
                  index: Literal['a', 'b', 'c', 'd', 'e', '1', '2', '3', '4', '5']):
        '''Makes a bid at a given row or column during the draft, i.e. `/draft gib puissance e`.
        '''
        async with self.lock:
            if self.gib_time == False:
                await ctx.send("Bids are already in, sorry.", ephemeral=True)
                return
            if self.game_time == False:
                await ctx.send("There is no active draft, sorry.", ephemeral=True)
                return
            if str(ctx.author.id) in self.players:
                if category[0] not in self.hands[str(ctx.author.id)]:
                    self.claims[str(ctx.author.id)] = self.get_claim(
                        category, index)
                    await ctx.send("Understood.", ephemeral=True)
                else:
                    await ctx.send("Invalid bid, please check for typos. Example input: `/draft gib puissance b`", ephemeral=True)
                if len(self.claims.keys()) == len(self.players) and self.adding_time == False:
                    self.gib_time = False
                    await self.resolve()
            else:
                await ctx.send("You are not a player in the current draft.", ephemeral=True)

    @draft.command()
    async def active(self, ctx):
        if self.no_new == False:
            await ctx.send("There's no active draft.")
            return
        await ctx.send(f'Active draft was started in {self.ctx.channel.mention}: https://docs.google.com/spreadsheets/d/{self.current_ID}')

    @draft.command()
    async def help(self, ctx, command: Literal['gib', 'clash', 'choose', 'lesgo', 'generate', 'add_me', 'remove_me', 'reset', 'draft'] = 'draft'):
        await ctx.send(self.help[command], ephemeral=True)
