from discord.ext import commands
from typing import Literal
import random


class HitLoc(commands.Cog):
    '''Random and semi-random hit locations
    '''

    def __init__(self):
        self.chakra = [
            'Crown',
            'Brow',
            'Throat',
            'Heart',
            'Solar Plexus',
            'Sacrum',
            'Root'
        ]

        self.hl_wounds = {
            'Head': [
                'Head',
                'Neck',
                'Left Shoulder',
                'Right Shoulder',
                'Miss',
                'Miss',
            ],
            'Neck': [
                'Head',
                'Neck',
                'Left Shoulder',
                'Right Shoulder',
                'Upper Torso',
                'Miss',
            ],
            'Left Shoulder': [
                'Head',
                'Neck',
                'Left Shoulder',
                'Left Arm',
                'Upper Torso',
                'Left Side'
            ],
            'Left Arm': [
                'Left Shoulder',
                'Left Arm',
                'Left Hand',
                'Upper Torso',
                'Left Side',
                'Miss',
            ],
            'Left Hand': [
                'Left Arm',
                'Left Hand',
                'Left Side',
                'Left Hip',
                'Miss',
                'Miss',
            ],
            'Right Shoulder': [
                'Head',
                'Neck',
                'Right Shoulder',
                'Right Arm',
                'Upper Torso',
                'Right Side'
            ],
            'Right Arm': [
                'Right Shoulder',
                'Right Arm',
                'Right Hand',
                'Upper Torso',
                'Right Side',
                'Miss',
            ],
            'Right Hand': [
                'Right Arm',
                'Right Hand',
                'Right Side',
                'Right Hip',
                'Miss',
                'Miss',
            ],
            'Upper Torso': [
                'Upper Torso',
                'Right Shoulder',
                'Left Shoulder',
                'Right Side',
                'Left Side',
                'Lower Torso',
            ],
            'Left Side': [
                'Left Side',
                'Left Arm',
                'Upper Torso',
                'Lower Torso',
                'Left Hip',
                'Left Hand',
            ],
            'Right Side': [
                'Right Side',
                'Right Arm',
                'Upper Torso',
                'Lower Torso',
                'Right Hip',
                'Right Hand',
            ],
            'Lower Torso': [
                'Lower Torso',
                'Left Hip',
                'Right Hip',
                'Groin',
                'Right Side',
                'Left Side'
            ],
            'Left Hip': [
                'Left Hip',
                'Left Side',
                'Left Hand',
                'Groin',
                'Lower Torso',
                'Left Leg'
            ],
            'Right Hip': [
                'Right Hip',
                'Right Side',
                'Right Hand',
                'Groin',
                'Lower Torso',
                'Right Leg'
            ],
            'Groin': [
                'Groin',
                'Lower Torso',
                'Left Hip',
                'Right Hip',
                'Left Leg',
                'Right Leg',
            ],
            'Left Leg': [
                'Left Leg',
                'Left Foot',
                'Groin',
                'Left Hip',
                'Right Leg',
                'Miss'
            ],
            'Right Leg': [
                'Right Leg',
                'Right Foot',
                'Groin',
                'Right Hip',
                'Left Leg',
                'Miss'
            ],
            'Left Foot': [
                'Left Foot',
                'Right Foot',
                'Left Leg',
                'Right Leg',
                'Miss',
                'Miss',
            ],
            'Right Foot': [
                'Left Foot',
                'Right Foot',
                'Left Leg',
                'Right Leg',
                'Miss',
                'Miss',
            ],
        }

    @commands.hybrid_group(fallback='unaimed')
    async def hitloc(self, ctx: commands.Context) -> None:
        '''Roll a random body part'''
        roll = random.choice(list(self.hl_wounds.keys()))
        await ctx.send(f'Hit Location: {roll}')

    @hitloc.command()
    async def aim(self, ctx, target: Literal['head', 'neck', 'left shoulder', 'left arm', 'left hand',
                                             'right shoulder', 'right arm', 'right hand', 'upper torso',
                                             'left side', 'right side', 'lower torso', 'left hip', 'right hip',
                                             'groin', 'left leg', 'left foot', 'right leg', 'right foot']):
        '''Try to hit a specific part, might miss'''
        target = ' '.join([t.capitalize() for t in target.split(' ')])
        if target not in self.hl_wounds.keys():
            await ctx.send('Error: Invalid aimed target.')
        else:
            roll = random.choice(self.hl_wounds[target])
            await ctx.send(f'Hit Location: {roll} (Aimed: {target})')

    @hitloc.command()
    async def upper(self, ctx):
        '''Roll a random upper body part'''
        roll = random.choice(list(self.hl_wounds.keys())[:-4])
        await ctx.send(f'Hit Location: {roll}')

    @hitloc.command()
    async def lower(self, ctx):
        '''Roll a random lower body part'''
        roll = random.choice(list(self.hl_wounds.keys())[11:])
        await ctx.send(f'Hit Location: {roll}')

    @hitloc.command()
    async def chakra(self, ctx):
        '''Roll a random chakra'''
        roll = random.choice(self.chakra)
        await ctx.send(f'Chakra Hit Location: {roll}')
